https://how-to.dev/how-to-create-commit-automatically-in-a-merge-request-in-gitlab

```yaml
stages:
  - test

add-commit:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: never
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      when: manual
  script:
    - echo 'test' >> $CI_COMMIT_BRANCH
    - git add .
    - git status
    - git -c user.email="$GITLAB_USER_EMAIL" -c user.name="$GITLAB_USER_NAME" commit -m "add change in $CI_PIPELINE_ID"
    - git push "https://gitlab-ci-token:$GITLAB_PUSH_TOKEN@gitlab.com/marcin-wosinek/automated-commit.git" HEAD:$CI_COMMIT_BRANCH
```

https://blog.jdriven.com/2021/03/maven-release-on-gitlab/

```yaml
release:
  image: openjdk:15
  stage: release
  only:
    - master
  when: manual
  before_script:
    - mkdir -p ~/.ssh/
    - cp $DEPLOY_PRIVATE_KEY ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
    - cp $KNOWN_HOSTS ~/.ssh/known_hosts
    - apt-get update && apt-get install -y git
    - git config --global user.email "noreply@your.gitlab.host"
    - git config --global user.name "GitLab CI"
    - git checkout -B "$CI_COMMIT_REF_NAME"
  script:
    - ./mvnw release:prepare release:perform

```
